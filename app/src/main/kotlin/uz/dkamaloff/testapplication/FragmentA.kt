package uz.dkamaloff.testapplication

import android.graphics.Color
import android.os.Bundle
import kotlinx.android.synthetic.main.fr_a.*
import uz.dkamaloff.testapplication.alert.alert


/**
 * @author @ddk9499
 */

class FragmentA : BaseFragment() {
    override val layoutRes = R.layout.fr_a

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        next_screen_btn.setOnClickListener { _ ->
            alert {
                title = "Some title"
                messageResource = R.string.fragment_b
                negativeButton("Negative") { it.dismiss() }
                positiveButton("Positive") { activity?.replaceFragment(FragmentB(), hasBackStackTag = true) }
                neutralPressed("Neutral") {
                    showToastMessage("Neutral button clicked")
                    it.dismiss()
                }
                show()
            }
        }

        toolbar.inflateMenu(R.menu.menu_a)
        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.a_menu_item -> showSnackMessage(it.title.toString())
            }
            true
        }
    }

    override fun onResume() {
        super.onResume()
        activity?.window?.statusBarColor = Color.TRANSPARENT
    }

    override fun onBackPressed() {
        activity?.finish()
    }
}