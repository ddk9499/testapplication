package uz.dkamaloff.testapplication

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class AppActivity : AppCompatActivity() {

    private val currentFragment
        get() = supportFragmentManager.findFragmentById(R.id.main_container) as BaseFragment?

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app)

        replaceFragment(FragmentA())
    }

    override fun onBackPressed() {
        currentFragment?.onBackPressed() ?: super.onBackPressed()
    }
}
