@file:Suppress("NOTHING_TO_INLINE")

package uz.dkamaloff.testapplication.alert

import android.content.Context
import android.content.DialogInterface
import android.support.v4.app.Fragment


/**
 * @author @ddk9499
 */

fun Context.alert(init: AlertBuilder<DialogInterface>.() -> Unit): AlertBuilder<DialogInterface> =
        AndroidAlertBuilder(this).apply { init() }

inline fun Fragment.alert(noinline init: AlertBuilder<DialogInterface>.() -> Unit) = activity?.alert(init)