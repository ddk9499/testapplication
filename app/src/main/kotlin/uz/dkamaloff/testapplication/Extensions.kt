package uz.dkamaloff.testapplication

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction


/**
 * @author @ddk9499
 */

inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> Unit) {
    val fragmentTransaction = beginTransaction()
    fragmentTransaction.func()
    fragmentTransaction.commit()
}

fun FragmentActivity.replaceFragment(fragment: Fragment, frameId: Int = R.id.main_container, hasBackStackTag: Boolean = false) =
        supportFragmentManager.inTransaction {
            replace(frameId, fragment)
            if (hasBackStackTag) addToBackStack(fragment.javaClass.name)
        }