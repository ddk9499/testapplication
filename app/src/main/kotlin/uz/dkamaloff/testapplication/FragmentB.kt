package uz.dkamaloff.testapplication

import android.os.Bundle
import android.support.v4.content.ContextCompat
import kotlinx.android.synthetic.main.fr_b.*


/**
 * @author @ddk9499
 */

class FragmentB : BaseFragment() {
    override val layoutRes = R.layout.fr_b

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        toolbar.inflateMenu(R.menu.menu_b)
        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.b_menu_item -> showSnackMessage(it.title.toString())
            }
            true
        }
    }

    override fun onResume() {
        super.onResume()
        activity?.apply {
            window?.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
        }
    }

    override fun onBackPressed() {
        showToastMessage("FragmentB is closed.")
        fragmentManager?.popBackStackImmediate()
    }
}